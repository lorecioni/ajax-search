Ajax Search
===========

Search interface with word hints based on Ajax calls. 

Every word has an occurrency attribute that explain how many times it has been searched. Results are ordered by occurrency (desc) and by value (asc).
Dataset to be expanded.
